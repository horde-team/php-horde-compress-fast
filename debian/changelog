php-horde-compress-fast (1.1.1-11) UNRELEASED; urgency=medium

  * d/patches: Add header to 1010_phpunit-8.x+9.x.patch.
  * d/watch: Switch to format version 4.
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 19 Jan 2023 08:54:37 +0100

php-horde-compress-fast (1.1.1-10) unstable; urgency=medium

  * d/t/control: Require php-horde-lz4.
  * d/patches: Add debian/patches/1010_phpunit-8.x+9.x.patch.
    Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 22 Jul 2020 11:20:42 +0200

php-horde-compress-fast (1.1.1-9) unstable; urgency=medium

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:27:47 +0200

php-horde-compress-fast (1.1.1-8) unstable; urgency=medium

  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 06 Jun 2020 00:36:18 +0200

php-horde-compress-fast (1.1.1-7) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959247).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:13:28 +0200

php-horde-compress-fast (1.1.1-6) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 07:51:40 +0200

php-horde-compress-fast (1.1.1-5) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Mon, 14 May 2018 23:48:51 +0200

php-horde-compress-fast (1.1.1-4) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 11:06:45 +0200

php-horde-compress-fast (1.1.1-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 22:03:19 +0200

php-horde-compress-fast (1.1.1-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 22:28:52 +0100

php-horde-compress-fast (1.1.1-1) unstable; urgency=medium

  * New upstream version 1.1.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 13:57:09 +0100

php-horde-compress-fast (1.1.0-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:06:31 +0200

php-horde-compress-fast (1.1.0-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:12:14 +0200

php-horde-compress-fast (1.1.0-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 1.1.0

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:04:22 +0200

php-horde-compress-fast (1.0.3-5) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 11:51:13 +0200

php-horde-compress-fast (1.0.3-4) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 09:58:54 +0200

php-horde-compress-fast (1.0.3-3) unstable; urgency=medium

  * Fix DEP-8 depends

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 21:28:51 +0200

php-horde-compress-fast (1.0.3-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 24 Aug 2014 09:16:46 +0200

php-horde-compress-fast (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 Jul 2014 23:16:39 +0200

php-horde-compress-fast (1.0.2-1) unstable; urgency=low

  * New upstream version 1.0.2

 -- Mathieu Parent <sathieu@debian.org>  Sat, 10 Aug 2013 19:59:01 +0200

php-horde-compress-fast (1.0.1-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 18:43:16 +0200

php-horde-compress-fast (1.0.1-1) unstable; urgency=low

  * Horde's Horde_Compress_Fast package
  * Initial packaging (Closes: #704856)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 16:03:43 +0200
